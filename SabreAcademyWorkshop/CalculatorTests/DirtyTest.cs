﻿using NUnit.Framework;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;

namespace SabreAcademyWorkshop.CalculatorTests
{
    public class DirtyTest
    {
        [Test]
        public void Test()
        {
            // Open application
            var application = Application.Launch("calc.exe");
            
            // find window on desktop 
            var mainWindow = application.GetWindow("Calculator", InitializeOption.NoCache);

            // find button "1"
            var oneBtn = mainWindow.Get<Button>("1"); //default search mechanism is by text

            // find button "3"
            var threeBtn = mainWindow.Get<Button>(SearchCriteria.ByText("3")); //same as above searchCriteria declared

            // find button "="
            var equalsBtn = mainWindow.Get<Button>(SearchCriteria.ByAutomationId("121"));

            // find button "+"
            var plusBtn = mainWindow.Get<Button>(SearchCriteria.ByAutomationId("93").AndByClassName("Button"));

            var result = mainWindow.Get(SearchCriteria.ByAutomationId("158"));

            // initial panel value = 0
            //Assert.That(result.Name, Is.EqualTo("0"));
            
            // Execute test case 1+3=4
            oneBtn.Click();
            plusBtn.Click();
            threeBtn.Click();
            equalsBtn.Click();
            
            // panel value after operation = 4
            Assert.That(result.Name, Is.EqualTo("4"));

        }

    }
}
