﻿using NUnit.Framework;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.ScreenObjects;

namespace SabreAcademyWorkshop.CalculatorTests
{
    /// <summary>
    /// A test class for ...
    /// </summary>
    [TestFixture]
    public class CalculatorTest
    {
        public static CalculatorWindow CalculatorWindow;

        public static Application Application;

        #region Setup and Tear down
        /// <summary>
        /// This runs only once at the beginning of all tests and is used for all tests in the 
        /// class.
        /// </summary>
        [TestFixtureSetUp]
        public void InitialSetup()
        {
            Application = Application.Launch("calc.exe");
            var screenRepo = new ScreenRepository(Application);
            CalculatorWindow = screenRepo.Get<CalculatorWindow>("Calculator", InitializeOption.NoCache); //!! for polish Windows it will be Kalkulator
        }

        /// <summary>
        /// This runs only once at the end of all tests and is used for all tests in the class.
        /// </summary>
        [TestFixtureTearDown]
        public void FinalTearDown()
        {
            CalculatorWindow.Close();
            Assert.That(CalculatorWindow.IsClosed);
        }

        /// <summary>
        /// This setup functions runs before each test method
        /// </summary>
        [SetUp]
        public void SetupForEachTest()
        {
            CalculatorWindow.btnClear.Click();
        }

        /// <summary>
        /// This setup functions runs after each test method
        /// </summary>
        [TearDown]
        public void TearDownForEachTest()
        {
            
        }
        #endregion

        [Test]
        public void TestShouldPass()
        {
            // Step 1 - Arrange

            // initial panel value should be 0
            Assert.That(CalculatorWindow.GetCalculationResult, Is.EqualTo("0"));

            // Step 2 - Act
            // Execute test case 1+1=2
            CalculatorWindow.btnOne.Click();
            CalculatorWindow.btnPlus.Click();
            CalculatorWindow.btnThree.Click();
            CalculatorWindow.btnEqual.Click();

            // Step 3 - Assert  
            // panel value after operation = 4
            Assert.That(CalculatorWindow.GetCalculationResult, Is.EqualTo("4"));

        }

        [Test]
        public void TestShouldFail()
        {
            // Step 1 - Arrange

            // initial panel value should be 0
            Assert.That(CalculatorWindow.GetCalculationResult, Is.EqualTo("0"));

            // Step 2 - Act
            // Execute test case 3-1=3
            CalculatorWindow.btnThree.Click();
            CalculatorWindow.btnMinus.Click();
            CalculatorWindow.btnOne.Click();
            CalculatorWindow.btnEqual.Click();

            // Step 3 - Assert  
            // panel value after operation = 4
            Assert.That(CalculatorWindow.GetCalculationResult, Is.EqualTo("4"));

        }
    }
}
