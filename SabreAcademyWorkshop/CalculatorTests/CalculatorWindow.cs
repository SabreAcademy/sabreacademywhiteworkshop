﻿using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems;
using TestStack.White.UIItems.WindowItems;

namespace SabreAcademyWorkshop.CalculatorTests
{
    public class CalculatorWindow : AppScreen
    {

        [AutomationId("131")]
        public Button btnOne;

        [AutomationId("132")]
        public Button btnTwo;

        [AutomationId("133")]
        public Button btnThree;

        [AutomationId("134")]
        public Button btnFour;

        [AutomationId("135")]
        public Button btnFive;

        [AutomationId("136")]
        public Button btnSix;

        [AutomationId("137")]
        public Button btnSeven;

        [AutomationId("138")]
        public Button btnEight;

        [AutomationId("139")]
        public Button btnNine;

        [AutomationId("130")]
        public Button btnZero;

        [AutomationId("93")]
        public Button btnPlus;
        
        [AutomationId("94")]
        public Button btnMinus;

        [AutomationId("121")]
        public Button btnEqual;

        [AutomationId("158")]
        private Label _pnlResult;

        [AutomationId("125")]
        public Button btnMemoryAdd;

        [AutomationId("126")]
        public Button btnMemorySubtract;

        [AutomationId("122")]
        public Button btnMemoryClear;

        [AutomationId("123")]
        public Button btnMemoryRecall;

        [AutomationId("124")]
        public Button btnMemoryStore;

        [AutomationId("81")]
        public Button btnClear;

        public virtual string GetCalculationResult
        {
            get { return _pnlResult.Name; }
        }

        public CalculatorWindow(Window window, ScreenRepository screenRepository) : base(window, screenRepository)
        {
        }
    }
}
