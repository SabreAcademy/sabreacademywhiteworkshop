﻿using NUnit.Framework;
using TestStack.White;
using TestStack.White.Factory;
using TestStack.White.ScreenObjects;

namespace SabreAcademyWorkshop.DemoApplication
{
    [TestFixture]
    public class SabreAcademyWorkshop
    {
        public static SabreAcademyWorkshopWindow SAWorkshopWnd;
        public static Application Application;

        #region Setup and Tear down

        [TestFixtureSetUp]
        public void InitialSetup ()
        {
        }

        [TestFixtureTearDown]
        public void FinalTearDown ()
        {
        }

        [SetUp]
        public void SetupForEachTest ()
        {
            Application = Application.Launch ( @"C:\temp\SabreAcademyWorkshop\SabreAcademyWorkshop.exe" );
            var screenRepo = new ScreenRepository ( Application );
            SAWorkshopWnd = screenRepo.Get<SabreAcademyWorkshopWindow> ( "SabreAcademyWorkshop", InitializeOption.NoCache );
        }

        [TearDown]
        public void TearDownForEachTest ()
        {
            SAWorkshopWnd.Close ();
            Application.Dispose ();
        }

        #endregion


        #region Tests

        [Test]
        public void TC01_SelectGroup1 ()
        {
            SAWorkshopWnd.rdoGroup1.Select ();

            Assert.IsTrue ( SAWorkshopWnd.btnChangeLabel.Enabled );
            Assert.IsTrue ( SAWorkshopWnd.lblLabel.Enabled );
            Assert.IsTrue ( SAWorkshopWnd.chkDisableButton.Enabled );
            Assert.IsTrue ( SAWorkshopWnd.grpGroup1.Enabled );

            Assert.IsFalse ( SAWorkshopWnd.cmbComboBox.Enabled );
            Assert.IsFalse ( SAWorkshopWnd.txtTextBox.Enabled );
            Assert.IsFalse ( SAWorkshopWnd.btnAdd.Enabled );
            Assert.IsTrue ( SAWorkshopWnd.grpGroup2.Enabled );
        }


        [Test]
        public void TC02_SelectGroup2 ()
        {
            SAWorkshopWnd.rdoGroup2.Select ();

            Assert.IsFalse ( SAWorkshopWnd.btnChangeLabel.Enabled );
            Assert.IsFalse ( SAWorkshopWnd.lblLabel.Enabled );
            Assert.IsFalse ( SAWorkshopWnd.chkDisableButton.Enabled );
            Assert.IsTrue ( SAWorkshopWnd.grpGroup1.Enabled );

            Assert.IsTrue ( SAWorkshopWnd.cmbComboBox.Enabled );
            Assert.IsTrue ( SAWorkshopWnd.txtTextBox.Enabled );
            Assert.IsTrue ( SAWorkshopWnd.btnAdd.Enabled );
            Assert.IsTrue ( SAWorkshopWnd.grpGroup2.Enabled );
        }


        [Test]
        [Description ( "Defect - btnChangeLabel is not disabled " )]
        public void TC03_VerifyCheckBox ()
        {
            SAWorkshopWnd.rdoGroup1.Select ();
            SAWorkshopWnd.chkDisableButton.Select ();

            Assert.IsFalse ( SAWorkshopWnd.btnChangeLabel.Enabled );

            SAWorkshopWnd.chkDisableButton.UnSelect ();

            Assert.IsTrue ( SAWorkshopWnd.btnChangeLabel.Enabled );
        }


        [Test]
        public void TC04_VerifyChangeLabelBtn ()
        {
            SAWorkshopWnd.rdoGroup1.Select ();
            SAWorkshopWnd.chkDisableButton.UnSelect ();

            Assert.AreEqual ( "NO", SAWorkshopWnd.lblLabel.Text );

            SAWorkshopWnd.btnChangeLabel.Click ();

            Assert.AreEqual ( "YES", SAWorkshopWnd.lblLabel.Text );

            SAWorkshopWnd.btnChangeLabel.Click ();

            Assert.AreEqual ( "NO", SAWorkshopWnd.lblLabel.Text );
        }


        [Test]
        public void TC05_VerifyComboBox ()
        {
            SAWorkshopWnd.rdoGroup2.Select ();

            Assert.AreEqual ( 3, SAWorkshopWnd.cmbComboBox.Items.Count );

            SAWorkshopWnd.cmbComboBox.Select ( 1 );

            Assert.AreEqual ( SAWorkshopWnd.cmbComboBox.EditableText, SAWorkshopWnd.txtTextBox.Text );
        }


        [Test]
        [Description ( "Defect - Information popup instead of Error" )]
        public void TC06_AddExistingItem ()
        {
            SAWorkshopWnd.rdoGroup2.Select ();

            SAWorkshopWnd.cmbComboBox.Select ( 1 );
            SAWorkshopWnd.btnAdd.Click ();
            SAWorkshopWnd.ClosePopupWindow ( "Error" );

            Assert.AreEqual ( 3, SAWorkshopWnd.cmbComboBox.Items.Count );
        }


        [Test]
        public void TC07_AddNewItem ()
        {
            SAWorkshopWnd.rdoGroup2.Select ();

            Assert.AreEqual ( 3, SAWorkshopWnd.cmbComboBox.Items.Count );

            SAWorkshopWnd.txtTextBox.Text = "New item";
            SAWorkshopWnd.btnAdd.Click ();
            SAWorkshopWnd.ClosePopupWindow ( "Information" );

            Assert.AreEqual ( 4, SAWorkshopWnd.cmbComboBox.Items.Count );
            Assert.AreEqual ( "New item", SAWorkshopWnd.cmbComboBox.Items [3].Text );

            SAWorkshopWnd.txtTextBox.Click ();
        }


        [Test]
        public void TC08_VerifyVersion ()
        {
            Assert.AreEqual ( "version 3.0", SAWorkshopWnd.ssStatusBar.Items [2].Name );
        }
        #endregion
    }
}
