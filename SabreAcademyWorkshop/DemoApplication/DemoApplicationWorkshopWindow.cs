﻿using TestStack.White.Factory;
using TestStack.White.ScreenObjects;
using TestStack.White.ScreenObjects.ScreenAttributes;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.ListBoxItems;
using TestStack.White.UIItems.WindowItems;
using TestStack.White.UIItems.WindowStripControls;

namespace SabreAcademyWorkshop.DemoApplication
{
    public class SabreAcademyWorkshopWindow : AppScreen
    {
        [AutomationId ( "btnChangeLabel" )]
        public Button btnChangeLabel;

        [AutomationId ( "btnAdd" )]
        public Button btnAdd;


        [AutomationId ( "rdoGroup1" )]
        public RadioButton rdoGroup1;

        [AutomationId ( "rdoGroup2" )]
        public RadioButton rdoGroup2;


        [AutomationId ( "grpGroup1" )]
        public GroupBox grpGroup1;

        [AutomationId ( "grpGroup2" )]
        public GroupBox grpGroup2;


        [AutomationId ( "chkDisableButton" )]
        public CheckBox chkDisableButton;


        [AutomationId ( "lblLabel" )]
        public Label lblLabel;


        [AutomationId ( "cmbComboBox" )]
        public ComboBox cmbComboBox;


        [AutomationId ( "txtTextBox" )]
        public TextBox txtTextBox;

        [AutomationId ( "ssStatusBat" )]
        public StatusStrip ssStatusBar;

        public SabreAcademyWorkshopWindow ( Window window, ScreenRepository screenRepository )
            : base ( window, screenRepository )
        {
        }

        public virtual void ClosePopupWindow ( string title )
        {
            this.Window.ModalWindow ( SearchCriteria.ByText ( title ), InitializeOption.WithCache ).Close ();
        }
    }
}
