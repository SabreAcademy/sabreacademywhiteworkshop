﻿using System;
using NUnit.Framework;

namespace SabreAcademyWorkshop.nUnitBasics
{
    /// <summary>
    /// A test class for ...
    /// </summary>

    [TestFixture]
    public class MainTestClass
    {

        #region Setup and Tear down
        /// <summary>
        /// This runs only once at the beginning of all tests and is used for all tests in the 
        /// class.
        /// </summary>
        [TestFixtureSetUp]
        public void TestSetup()
        {
            Console.WriteLine("+ Hello from TestFixtureSetUp");
        }

        /// <summary>
        /// This runs only once at the end of all tests and is used for all tests in the class.
        /// </summary>
        [TestFixtureTearDown]
        public void FinalTearDown()
        {
            Console.WriteLine("+ Hello from TestFixtureTearDown");
        }

        /// <summary>
        /// This setup functions runs before each test method
        /// </summary>
        [SetUp]
        public void SetupForEachTest()
        {
            Console.WriteLine("++ Hello from SetUp");
        }

        /// <summary>
        /// This setup functions runs after each test method
        /// </summary>
        [TearDown]
        public void TearDownForEachTest()
        {
            Console.WriteLine("++ Hello from TearDown");
        }
        #endregion

        [Test]
        public void FirstTest()
        {
            // Step 1 - Arrange
            Console.WriteLine(" Hello from FirstTest");
            // Step 2 - Act

            // Step 3 - Assert  
            Assert.IsTrue(true);
        }
        
        [Test]
        public void SecondTest()
        {
            // Step 1 - Arrange
            Console.WriteLine(" Hello from SecondTest");
            // Step 2 - Act

            // Step 3 - Assert  
            Assert.IsTrue(false);
        }


    }
}
