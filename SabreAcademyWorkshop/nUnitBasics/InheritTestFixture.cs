﻿using System;
using NUnit.Framework;

namespace SabreAcademyWorkshop.nUnitBasics
{
    class InheritTestFixture :MainTestClass
    {
        [TestCase(12, 3, 4)]
        [TestCase(12, 2, 6)]
        [TestCase(12, 4, 3)]
        public void DivideTest(int n, int d, int q)
        {
            Console.WriteLine(" Hello from DivideTest");
            Assert.AreEqual(q, n / d);

        }

        [TestCase(12, 3, Result = 4)]
        [TestCase(12, 2, Result = 6)]
        [TestCase(12, 4, Result = 3)]
        public int DivideTest(int n, int d)
        {
            Console.WriteLine(" Hello from DivideTest return");
            return (n / d);
        }
    }
}
