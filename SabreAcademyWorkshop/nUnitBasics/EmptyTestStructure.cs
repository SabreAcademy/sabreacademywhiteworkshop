﻿using NUnit.Framework;

namespace SabreAcademyWorkshop.nUnitBasics
{
    /// <summary>
    /// A test class for ...
    /// </summary>
    [TestFixture]
    public class EmptyTestStructure
    {
        #region Setup and Tear down
        /// <summary>
        /// This runs only once at the beginning of all tests and is used for all tests in the 
        /// class.
        /// </summary>
        [TestFixtureSetUp]
        public void InitialSetup()
        {

        }

        /// <summary>
        /// This runs only once at the end of all tests and is used for all tests in the class.
        /// </summary>
        [TestFixtureTearDown]
        public void FinalTearDown()
        {

        }

        /// <summary>
        /// This setup functions runs before each test method
        /// </summary>
        [SetUp]
        public void SetupForEachTest()
        {
        }

        /// <summary>
        /// This setup functions runs after each test method
        /// </summary>
        [TearDown]
        public void TearDownForEachTest()
        {
        }
        #endregion

        [Test]
        public void FirstTest()
        {
            // Step 1 - Arrange

            // Step 2 - Act

            // Step 3 - Assert        
        }
    }
}
